#zip_iterator#


###Rationale:###
Boost zip iterator does not work for sorting mixed containers.

###Requirements:###
C++11

###Docs:###
as tiny as they may be..
Supports (or supposed to) full iterator functions; any deviation from standard mandated iterator functions is considered a bug and should be reported.


Three typical iterators are supported:

 Forward
 
 Bidirectional
 
 Random
 
 

To access/create one of each type call:

zip::random(itr_1,itr_2,itr_3,...);



Iterators that have different terminal lengths are also supported.

zip::random<fuzzy_equality_tag>(itrs...) allows for only 1 iterator to meet a terminal condition e.g. == and != check all iterators and return true/false if any condition is satisfied.

zip::random<default_equality_tag>(itrs...) for normal behavior (default) /not required to be specified.

###Example:###

```
#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include "zip_iterator.h"

struct some_sortable_class
{
	int data;
	int id;
	some_sortable_class(int _data, int _id) : data(_data), id(_id) {}
	some_sortable_class() {}
	bool operator <(const some_sortable_class& other) const
	{
		return data < other.data;
	}
	inline friend void swap(some_sortable_class& left, some_sortable_class& right) noexcept
	{
		auto tmp = left;
		left = right;
		right = tmp;
		std::cout << "swapped\n";
	}
};

//used as a later example
struct tiny_pair
{
	int x;
	float y;
};
int main()
{
	std::mt19937 rand_gen(0);
	std::vector<int> ints_1;
	std::vector<some_sortable_class> sortable_class;
	std::vector<char> ints_3;
	for (int i = 0; i < 10; i++)
	{
		ints_1.push_back(10 - i);
		sortable_class.push_back(some_sortable_class(10 - i, rand_gen()&0xFF));
		ints_3.push_back('A' + 9 - i);
	}
	//magic c++14 variadic auto lambdas.
	auto sort_vecs = [](auto& ... vecs)
	{
		std::sort(zip::random(vecs.begin()...), zip::random(vecs.end()...));
	};
	sort_vecs(sortable_class,ints_1,ints_3);
	std::cout << "First vector: \n";
	for (auto& i : ints_1)
	{
		std::cout << i << " | ";
	}
	std::cout << "\nSecond vector:\n";
	for (auto& i : sortable_class)
	{
		std::cout << "{" << i.data << "," << i.id << "} ";
	}
	std::cout << "\nThird vector:\n";
	for (auto& i : ints_3)
	{
		std::cout << i << " | ";
	}
	std::cout << '\n';

	//additional fun: you can zip types together when being passed off to iterator constructors.
	//aka this works
	std::vector<int> ints{ 1,2 };
	std::vector<float> floats{ 1.f,2.f };
	//works with pods, and regular objects all the same.
	//fed in iterator order must match expected construction or pod layout order.
	//e.g. {int,float} is your struct the iterators must also be {itr_int,itr_float}.
	std::vector<tiny_pair> combos(zip::random(ints.begin(), floats.begin()), zip::random(ints.end(), floats.end()));

}

```

###License###

Distributed under the Boost Software License, Version 1.0 [ http://www.boost.org/LICENSE_1_0.txt]( http://www.boost.org/LICENSE_1_0.txt)