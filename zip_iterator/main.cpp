#include "zip_iterator.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <memory>

struct some_sortable_class
{
	int data;
	int id;
	some_sortable_class(int _data, int _id) : data(_data), id(_id) {}
	some_sortable_class() {}
	bool operator <(const some_sortable_class& other) const
	{
		return data < other.data;
	}
	inline friend void swap(some_sortable_class& left, some_sortable_class& right) noexcept
	{
		auto tmp = left;
		left = right;
		right = tmp;
		std::cout << "swapped\n";
	}
};
int main()
{
	std::mt19937 rand_gen(0);
	std::vector<int> ints_1;
	std::vector<some_sortable_class> sortable_class;
	std::vector<std::unique_ptr<char>> ints_3;
	for (int i = 0; i < 10; i++)
	{
		ints_1.push_back(10 - i);
		sortable_class.push_back(some_sortable_class(10 - i, rand_gen() & 0xFF));
		ints_3.emplace_back(std::make_unique<char>('A' + 9 - i));
	}
	//magic c++14 variadic auto lambdas.
	auto sort_vecs = [](auto& ... vecs)
	{
		std::sort(zip::random(vecs.begin()...), zip::random(vecs.end()...));
	};
	sort_vecs(sortable_class, ints_1, ints_3);
	std::cout << "First vector: \n";
	for (auto& i : ints_1)
	{
		std::cout << i << " | ";
	}
	std::cout << "\nSecond vector:\n";
	for (auto& i : sortable_class)
	{
		std::cout << "{" << i.data << "," << i.id << "} ";
	}
	std::cout << "\nThird vector:\n";
	for (auto& i : ints_3)
	{
		std::cout << *i << " | ";
	}
	std::cout << '\n';

	//additional fun: you can zip types together when being passed off to iterator constructors.
	//aka this works
	std::vector<std::vector<int>> ints_move;
	for (int i = 0; i < 2; ++i)
	{
		ints_move.emplace_back(std::vector<int>{ i + 1 });
	}
	std::vector<float> floats_move;
	for (int i = 0; i < 2; ++i)
	{
		floats_move.emplace_back(i + 1.f);;
	}
	//works with pods, and regular objects all the same.
	//fed in iterator order must match expected construction or pod layout order.
	//e.g. {int,float} is your struct the iterators must also be {itr_int,itr_float}.
	struct tiny_pair_move_example
	{
		std::vector<int> x;
		float y;
	};
	for (auto begin = zip::random(ints_move.begin(), floats_move.begin()); begin != zip::random(ints_move.end(), floats_move.end()); ++begin)
	{
		//dereferences produce an rvalue
		//and we want to turn everything into an rvalue
		//but to have things work properly we somehow need this to be an lvalue....

		//potential fix: tag all default returned types with 'supposed to be lvalue tag'
		//offer a conversion operator that converts to 'supposed to be rvalue tag'
		//somehow allow the follow up conversion to automatically work.. ?
		tiny_pair_move_example pair = std::move(*begin);
	}
	std::vector<std::vector<int>> ints_copy{ { 1 },{ 2 } };
	std::vector<float> floats_copy{ 1.f,2.f };
	//works with pods, and regular objects all the same.
	//fed in iterator order must match expected construction or pod layout order.
	//e.g. {int,float} is your struct the iterators must also be {itr_int,itr_float}.
	struct tiny_pair_copy_example
	{
		std::vector<int> x;
		float y;
	};
	std::vector<tiny_pair_copy_example> tiny_pairs(zip::random(ints_copy.begin(), floats_copy.begin()), zip::random(ints_copy.end(), floats_copy.end()));
	auto itr_start = zip::random(ints_copy.begin(), floats_copy.begin());
	auto itr_next = itr_start + 1;

	//verifying that the standard expected thing is also allowed. aka std::move does indeed work with our returned type.
	//although it does require "cheating a little bit"(tm)
	//>std::move is specialized for our iterators dereferenced type; all we do is create an lvalue tagged type (since we are only ever dealing with rvalues)
	//and if we have called std::move on it we change the tag to rvalue
	using rvalue_tuple = decltype(std::move(*itr_next));
	using clvalue_tuple = const std::remove_reference<rvalue_tuple>::type&;

	rvalue_tuple rvalue_thing = std::move(*itr_next);
	clvalue_tuple const_ref_thing = rvalue_thing;	//not even a copy just looks like const <type>&; literally just a reference to our temporary

	tiny_pair_copy_example copy_from_rvalue = const_ref_thing;


	//alternatively: new convenience function
	std::vector<float> x_dir{ 1,2,3,4 }, y_dir{ 10,3,5,11,11 };
	std::cout << "simple structured binding demo\ndiffering length iterators auto end on the shortest length\n";
	for (const auto& [x, y] : zip::range(x_dir, y_dir))
	{
		std::cout << "x: " << x << "\ty: " << y <<"\n";
	}

	//everything should compile down to the same code when optimized.
	//for example: https://godbolt.org/z/6SRcXa
}